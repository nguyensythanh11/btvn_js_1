/**
 * Tính tổng 2 kí số
 * Input: Số có 2 chữ số
 * Các bước xử lý:
 *      - Tạo biến n để lưu số được nhập từ bàn phím
 *      - Tạo biến chuc, donvi để lưu kí số hàng chục, hàng đơn vị của n
 *      - Cách tính: chuc = Math.floor(n/10), donvi = n%10
 *      - Tạo biến result để lưu tổng của chuc và donvi
 * Output: In ra kết quả tổng hai kí số, result
 */

var n = 35;
var chuc = Math.floor(n/10);
var donvi = n%10;
var result = chuc + donvi;
console.log("Tổng hai kí số:",result);