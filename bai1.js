/**
 * Tính tiền lương nhân viên 
 * Input: Số ngày làm của người dùng
 * Các bước xử lý: 
 *      - Gán biến lương 1 ngày l là 100000
 *      - Gán biến số ngày làm của người dùng n là giá trị nhập từ bàn phím
 *      - Tạo ra biến result để lưu kết quả
 *      - Áp dụng công thức: Số tiền lương = Lương 1 ngày * số ngày làm
 * Output: Kết quả hiện thị: Số tiền lương
 */

var l = 100000;
var n = 6; // số ngày làm
var result = l*n;
console.log("Tiền lương nhân viên:", result);