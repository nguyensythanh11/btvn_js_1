/**
 * Quy đổi tiền
 * Input: số tiền USD
 * Các bước xử lý: 
 *      - Tạo ra biến USD để gán giá trị USD nhập từ bàn phím
 *      - Tạo ra biến VND để lưu kết quả
 *      - Áp dụng công thức: VND = USD*23.500
 * Đầu ra: In ra VND
 */

var USD = 2;
var VND = USD*23500;
console.log(VND,"VND");