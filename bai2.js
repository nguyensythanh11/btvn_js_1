/**
 * Tính giá trị trung bình
 * Input: 5 số thực
 * Các bước xử lý:
 *      - Tạo ra 5 biến a1, a2, a3, a4, a5 để gán lần lượt 5 giá trị nhập từ bàn phím
 *      - Tạo ra biến gttb để lưu kết quả
 *      - Tính giá trị trung bình bằng công thức: gttb = (a1+a2+a3+a4+a5)/5
 * Output: In ra giá trị trung bình: gttb
 */

var a1 = 1;
var a2 = 2;
var a3 = 3;
var a4 = 4;
var a5 = 5;
var gttb = (a1+a2+a3+a4+a5)/5
console.log("Giá trị trung bình:",gttb);
