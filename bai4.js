/**
 * Tính diện tích chu vi, hình chữ nhật 
 * Input: Chiều dài, chiều rộng hình chữ nhật
 * Các bước xử lý: 
 *      - Tạo biến dai, rong để lưu các giá trị chiều dài, chiều rộng nhập từ bàn phím
 *      - Tạo biến chuvi để lưu chu vi của hình chữ nhật
 *      - Áp dụng công thức: chuvi = (dai+rong)*2
 *      - Tạo biến dientich để lưu diện tích của hình chữ nhật
 *      - Áp dụng công thức: dientich = dai*rong
 * Output: In ra chu vi, diện tích hình chữ nhật
 */

var dai = 10;
var rong = 5;
var chuvi = (dai+rong)*2;
var dientich = dai*rong;
console.log("Chu vi hình chữ nhật:",chuvi);
console.log("Diện tích hình chữ nhật:",dientich);